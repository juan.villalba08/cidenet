const employees = [
    {
        primerApellido:"MARA",
        segundoApellido:"DONA",
        primerNombre:"Diego",
        otrosNombres:"Armando",
        paisDeEmpleo:"Colombia",
        tipoDeIdentificaion:"Cedula de Ciudadania",
        numeroDeIdentificacion:"1234567",
        correoElectronico:"diegoarmando@cidenet.co",
        fechaDeIngreso:"10/05/2021",
        area:"Talento Humano",
        estado:"Activo"
    },
    {
        primerApellido:"Ricardo",
        segundoApellido:"Garcia",
        primerNombre:"Don",
        otrosNombres:"Armando",
        paisDeEmpleo:"Estados Unidos",
        tipoDeIdentificaion:"Cedula de Ciudadania",
        numeroDeIdentificacion:1234567,
        correoElectronico:"RicardoGarcia@cidenet.co",
        fechaDeIngreso:"20/05/2021",
        area:"Finanzas",
        estado:"Activo"
    },
]

export default employees;