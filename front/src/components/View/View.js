import React,{useState, useEffect} from "react";
import "./View";
import axios from "axios";
import { useHistory } from "react-router";


const View = () =>{
const [empleados, setEmpleados] = useState();    
const [borrado,setBorrado] = useState();
const [id,setId] = useState("");
let history = useHistory()


useEffect(()=>{
    axios.get("http://localhost:3001/empleados")
    .then(response=>{
        const empleados = response.data
        setEmpleados(empleados);
    })
},[])

    const modificar = (e)=>{
        e.preventDefault();
        axios.put("http://localhost:3001/empleados")
        .then(response=>{
            console.log(response)
        })
    }

    const borrar = ( id)=>{
       const dataId= {id}
       console.log(dataId)
        axios.delete("http://localhost:3001/empleados",dataId)
        .then(response=>{
            console.log(response)
            const borrado = response
            setBorrado(borrado)

        })
    }




    return (
        <div className="view-container">

                  <table className="table table-hover col-12 text-center">
                      <thead className="table-dark">
                        <tr>
                            <th scope="col">Primer Apellido</th>
                            <th scope="col-1">Segundo Apellido</th>
                            <th scope="col-1">Primer Nombre</th>
                            <th scope="col-1">Segundo Nombre</th>
                            <th scope="col-1">Pais de Empleo</th>
                            <th scope="col-1">Tipo de ID</th>
                            <th scope="col-1">N° de ID</th>
                            <th scope="col-1">Correo Electronico</th>
                            <th scope="col-1">Editar</th>
                            <th scope="col-1">Borrar</th>

                        </tr>
                      </thead>
                      {empleados && empleados.map((el)=>{
                return(
                      <tbody key={el._id}>
                          <tr>
                              <th>{el.primerApellido}</th>
                              <th>{el.segundoApellido}</th>
                              <th>{el.primerNombre}</th>
                              <th>{el.otrosNombres}</th>
                              <th>{el.paisDeEmpleo}</th>
                              <th>{el.tipoDeIdentificaion}</th>
                              <th>{el.numeroDeIdentificacion}</th>
                              <th>{el.correoElectronico}</th>
                              <th><button onClick={(e)=>alert("modificar")}>✏️</button></th>
                              <th><button onClick={(e)=>borrar(el._id)}>❌</button></th>
                          </tr>
                      </tbody>
                )
            })}
                  </table>
        </div>
    )
}


export default View;