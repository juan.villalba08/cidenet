import React,{useState} from "react";
import "./register.css";
import axios from "axios";
import {useHistory} from "react-router-dom";
import {Formik, Form} from "formik"

const Register = ()=>{
    const [primerApellido, setPrimerApellido] = useState("");
    const [segundoApellido, setSegundoApellido] = useState("");
    const [primerNombre, setPrimerNombre] = useState("");
    const [otrosNombres, setOtrosNombres] = useState("");
    const [paisDeEmpleo, setPaisDeEmpleo] = useState("");
    const [tipoDeIdentificacion, setTipoDeIdentificacion] = useState("");
    const [numeroDeIdentificacion, setNumeroDeIdentificacion] = useState("");
    const [correoElectronico, setCorreoElectronico] = useState("");
    const [fechaDeIngreso, setFechaDeIngreso] = useState("");
    const [area,setArea] = useState("");
    const [estado, setEstado] = useState("activo")

    let history = useHistory();

    const empleadoRegistrado = {
        primerApellido,
        segundoApellido,
        primerNombre,
        otrosNombres, 
        paisDeEmpleo,
        tipoDeIdentificacion,
        numeroDeIdentificacion,
        correoElectronico,
        fechaDeIngreso,
        area,
        estado
    }

    const handleSubmit = (e)=>{
        e.preventDefault()
        console.log(empleadoRegistrado);
        axios.post("http://localhost:3001/register",{
            primerApellido:empleadoRegistrado.primerApellido,
            segundoApellido:empleadoRegistrado.segundoApellido,
            primerNombre:empleadoRegistrado.primerNombre,
            otrosNombres:empleadoRegistrado.otrosNombres, 
            paisDeEmpleo:empleadoRegistrado.paisDeEmpleo,
            tipoDeIdentificacion:empleadoRegistrado.tipoDeIdentificacion,
            numerodeIdentificacion:empleadoRegistrado.numeroDeIdentificacion,
            correoElectronico:empleadoRegistrado.correoElectronico,
            fechaDeIngreso:empleadoRegistrado.fechaDeIngreso,
            area:empleadoRegistrado.area,
            estado:empleadoRegistrado.estado
        })
       .then(response=>{
           response.json()
           window.location("/")
           
       })
       .catch(err=>{
        
       alert("el registro no se realizo correctamente, complete como corresponde los campos", err)
        })
    }

    return(
    <div className="register-container col-6">
        <h3>Registro de Empleados</h3>
        <form onSubmit={e=>handleSubmit()}>
            <div className="form-group m-1 needs-validation" >
                <label className="form-label" for="validationTooltip01">Primer Apellido</label>
                <input className="form-control" id="validationTooltip01" required 
                type="text" name="primerApellido"
                value={primerApellido} onChange={e=>setPrimerApellido(e.target.value)}
                 placeholder="Primer Apellido" maxLength="20"  />
                 <div className="valid-feedback"> Looks good!</div>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Segundo Apellido</label>
                <input className="form-control" id="validationTooltip02" required type="text" name="SegundoApellido"
                 placeholder="Segundo Apellido" value={segundoApellido} onChange={e=>setSegundoApellido(e.target.value)}
                maxLength="20"/>
                 <div className="valid-feedback"> Looks good!</div>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Primer Nombre</label>
                <input className="form-control" type="text" name="primerNombre"
                 value={primerNombre} onChange={e=>setPrimerNombre(e.target.value)}
                 placeholder="Primer Nombre" maxLength="20" required/>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Otros nombres</label>
                <input className="form-control" type="text" name="otrosnombres" 
                placeholder="Otros nombres" value={otrosNombres} onChange={e=>setOtrosNombres(e.target.value)}
                maxLength="20" required/>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Pais del Empleo</label>
                <select className="form-select" placeholder="Seleccione País" value={paisDeEmpleo} onChange={e=>setPaisDeEmpleo(e.target.value)}>
                    <option value="Colombia">Colombia</option>
                    <option value="Estados Unidos">Estados Unidos</option>
                </select>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Tipo de Identificación</label>
                <select className="form-select" placeholder="Seleccione el tipo" value={tipoDeIdentificacion} onChange={e=>setTipoDeIdentificacion(e.target.value)}>
                    <option value="Cedula de Ciudadania">Cedula de Ciudadania</option>
                    <option value="Cedula de Extranjeria">Cedula de Extrajenria</option>
                    <option value="Pasaporte">Pasaporte</option>
                    <option value="Permiso Especial">Permiso Especial</option>
                </select>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Numero de Identificación</label>
                <input className="form-control" type="text" name="id" placeholder="0303456ABR"
                value={numeroDeIdentificacion} onChange={e=>setNumeroDeIdentificacion(e.target.value)} 
                maxLength="20" required/>
            </div>
            <label className="form-label">Correo Electronico</label>
            <div className="input-group m-1">
                <input className="form-control" type="" placeholder="usuario" aria-label="usuario"
                value={correoElectronico} onChange={e=>setCorreoElectronico(e.target.value)}
                 aria-describedby="basic-addon2" maxLength="300" required/>
                <select className="form-select">
                    <option value="@cidenet.com.co">@cidenet.com.co</option>
                    <option value="@cidenet.com.us">@cidenet.com.us</option>
                </select>
            </div>
            <div className="form-group m-1">
                <label className="form-label">Fecha de Ingreso</label>
                <input className="form-control" type="date" name="date"
                value={fechaDeIngreso} onChange={e=>setFechaDeIngreso(e.target.value)}
                 min="2021-05-01" max="2022-01-01" required />
            </div>
            <div className="form-group m-1">
                <label className="form-label">Area</label>
                <select className="form-select" value={area} onChange={e=>setArea(e.target.value)}>
                    <option value="Administracion">Administración</option>
                    <option value="Financiera">Financiera</option>
                    <option value="Compras">Compras</option>
                    <option value="Infraestructura">Infraestructura</option>
                    <option value="Operario"> Operario</option>
                    <option value="Talento Humano">Talento Humano</option>
                    <option value="Servicios Varios">Servicios Varios</option>
                    <option value="Otro">Otro</option>
                </select>
            </div>
            <div className="form-group m-1">
                <label className="">Estado</label>
                <span className="input-group-text" id="basic-addon2" value="activo"onChange={e=>setEstado(e.target.value)}>Activo</span>
            </div>
        <button className="btn btn-primary" onClick={handleSubmit} type="submit">Registrar</button>
        </form>
        <Formik>
        {formik =>(
            <div>
                <h1 className="my-2 font-weight-bold-display-4">Registro</h1>
                <Form>

                </Form>
            </div>
        )

        }
        </Formik>
    </div>

    )
}

export default Register;