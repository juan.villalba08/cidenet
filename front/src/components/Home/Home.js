import React,{useState} from "react";
import "./Home.css";
import axios from "axios";
import { useHistory, NavLink } from "react-router-dom";


const Home = () => {
    const [view,setView] = useState();
let history = useHistory()

const viewEmployee = (e)=>{
    e.preventDefault()
    axios.get("http://localhost:3001/view")
    .then(response=>{
        console.log(response)
        const view = response
        setView(response)
        history.push("/vista")
    })
}

    return (
        <div classNameNameName="home-container">
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">Cidenet</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <NavLink to="/registro" className="nav-link">Registro</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/vista" className="nav-link" >Vista de empleados</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}


export default Home;