import React from "react";
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import Register from "./components/Register/Register";
import View from "./components/View/View";
import Home from "./components/Home/Home";

function App() {
  return (
    <BrowserRouter>
    <Route path="/" component={Home}/>
    <Route exact path="/registro" component={Register}/>
    <Route exact path="/vista" component={View}/>
    </BrowserRouter>
  );
}

export default App;
