const { Router } = require("express");
const registro = require("../models/Register.js");
const router = Router();

//Realizar registro
router.post("/register", async (req, res) => {
    const { primerApellido,
        segundoApellido,
        primerNombre,
        otrosNombres,
        paisDeEmpleo,
        tipoDeIdentificacion,
        numeroDeIdentificacion,
        correoElectronico,
        fechaDeIngreso,
        area,
        estado } = req.body;
    const nuevoEmpleado = new registro({
        primerApellido,
        segundoApellido,
        primerNombre,
        otrosNombres,
        paisDeEmpleo,
        tipoDeIdentificacion,
        numeroDeIdentificacion,
        correoElectronico,
        fechaDeIngreso,
        area,
        estado
    })
    await nuevoEmpleado.save();
    res.json({ status: "registro creado" });
})

//Consultar empleados
router.get("/empleados", async (req, res) => {
    const empleados = await registro.find();
    res.json(empleados);
});

//borrar por _id
router.delete("/empleados", async (req, res) => {
    const id= req.body._id
    console.log(id)
    const borrado = await registro.findByIdAndDelete(id);
    res.json({ status: "borrado", borrado })
})

module.exports = router;