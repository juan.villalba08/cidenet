const mongoose = require("mongoose");


const MONGODB_URI = MONGO_URI

mongoose.connect(MONGODB_URI,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false,
    useCreateIndex:true
})
.then((db)=>console.log("mongodb esta conectada"))
.catch((err)=>console.log(err));