const {Schema, model} = require("mongoose");

const registro = new Schema({
    primerApellido:{type:String, required:true},
    segundoApellido:{type:String, required:true},
    primerNombre:{type:String, required:true},
    otrosNombre:{type:String},
    paisDeEmpleo:{type:String},
    tipoDeIdentificacion:{type:String, enum:["Cedula de Ciudadania", "Cedula de Extranjeria", "Pasaporte", "Permiso Especial"]},
    numeroDeIdentificacion:{type:String, unique:true},
    correoElectronico:{type:String, unique:true},
    fechaDeIngreso:String,
    area:String,
    estado:String
},
{
    timestamps:true
});

/*
    primerApellido:{type:String, required:true},
    segundoApellido:{type:String, required:true},
    primerNombre:{type:String, required:true},
    otrosNombre:{type:String},
    paisDeEmpleo:{type:String, enum:["Colombia","USA"]},
    tipoDeId:{type:String, enum:["Cedula de Ciudadania", "Cedula de Extranjeria", "Pasaporte", "Permiso Especial"]},
    NumeroDeId:{type:String, unique:true},
    email:{type:String, unique:true},
    fechadeingreso:String,
    area:{type:String, enum:["Administracion", "Financiera", "Compras", "Infraestructura", "Operacion", "Talento Humano", "Servicios Varios","Otros"]}
    estado:String,
    fechaHoraIngreso:String*/ 

module.exports = model("Registro", registro);